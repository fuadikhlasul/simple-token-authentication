<!DOCTYPE html>
<html>
<head>
	<title>Simple Token-based Auth Simulator</title>

	<style type="text/css">
		body {
			font-family: Arial;
			background: #eee;
		}
		button {
			width: 100%;
			padding: 5px;
		}
		.wrapper {
			margin: 100px auto;
			width: 300px;
			padding: 20px;
			height: auto;
			background: #fff;
			box-shadow: 2px 2px 1px #ccc;
		}
	</style>
</head>
<body>

<div class="wrapper">
	<form method="GET" action="request.php">
		<input type="hidden" value="install" name="mode">
		<button type="submit">Click this button to install</button>
	</form>
</div>

</body>
</html>
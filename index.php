<?php @session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Token Verificator Simulation</title>

	<style type="text/css">
		body {
			font-family: Arial;
			background: #eee;
		}
		label, input, select {
			display: block;
			line-height: 1.9em;
			width: 100%;
		}
		input {
			padding: 2px
		}
		select {
			padding: 7px
		}
		.wrapper {
			margin: 100px auto;
			width: 300px;
			padding: 20px;
			height: auto;
			background: #fff;
			box-shadow: 2px 2px 1px #ccc;
		}
	</style>
</head>
<body>

<div class="wrapper">
	<form method="POST" action="request.php">
		<label>Masukkan Token :</label>
		<input type="text" name="token" id="token" autofocus>
		<br>
		<select name="command", id="command">
			<option value="req">Increment</option>
			<option value="auth">Authenticate</option>
		</select>
		<br>
		<input type="submit" name="send" value="Submit">
	</form>
	<?php if ( isset($_SESSION['server_response']) ) { ?>
		<br> <b> Server Response: </b> 
		<span id="server_response">
			<?= $_SESSION['server_response'] ?>
		</span>
	<?php unset($_SESSION['server_response']); } ?>
</div>

</body>
</html>
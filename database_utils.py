# !/usr/bin/env python
# -*- coding: utf-8 -*-
# system module
import MySQLdb

__author__ = 'fuadikhlasul@hotmail.com'

_db = MySQLdb.connect('localhost', 'root', '', 'simple_auth')

def create_table():
    _cursor = _db.cursor()
    _cursor.execute('DROP TABLE IF EXISTS `tokens`')
    sql = 'CREATE TABLE IF NOT EXISTS `tokens`(' \
          '`token` VARCHAR(9), ' \
          '`token_pair` VARCHAR(9), ' \
          '`count` INT(3)' \
          ') ENGINE=MyISAM'
    _cursor.execute(sql)

def seed_token_entry():
    import random
    from string import ascii_letters, digits

    _cursor = _db.cursor()
    _nrows = 10     # jumlah record data sementara di database
    _tlength = 9    # panjang string field token di database
    _plength = 9    # panjang string field token_pair di database

    for i in range(_nrows):
        _token = ''.join(random.SystemRandom().choice(ascii_letters + digits) for _ in range(_tlength))
        _token_pair = ''.join(random.SystemRandom().choice(ascii_letters + digits) for _ in range(_plength))
        sql = "INSERT INTO `tokens`(`token`, `token_pair`, `count`) VALUES('%s', '%s', 0)" %(_token, _token_pair)
        try:
            _cursor.execute(sql)
            _db.commit()
        except MySQLdb.MySQLError, e:
            print e
            _db.rollback()

def to_dict(cursor_):
    import itertools
    fields = [_[0].lower() for _ in cursor_.description]
    while True:
        rows = cursor_.fetchmany()
        if not rows: return
        for row in rows:
            yield dict(itertools.izip(fields, row))

if __name__ == '__main__':
    create_table()
    seed_token_entry()
    _db.close()
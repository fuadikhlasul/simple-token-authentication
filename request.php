<?php 

$method = $_SERVER['REQUEST_METHOD'];
$node = 'auth';
$ch = curl_init();

if ('get' === strtolower($method)) {
	if ( isset($_GET['mode']) && 'install' === strtolower($_GET['mode']) ) {
		# in-case of database initialization by the server
		$node = $_GET['mode'];
	}
} else {	
	if ( isset($_POST['send']) ) {
		# unset variable $_POST['send'] because it's not necessary for client request to server
		unset($_POST['send']);
		# convert array post data into url-encoded string argument data like:
		# "http://server_hostname:port/node?vararg1=val1&vararg2=val2"
		$args = '';
		foreach ($_POST as $k => $v) {
			$args .= "$k=$v&";
		}
		# remove last ampersend ("&") in arguments string
		$args = rtrim($args, '&');
		# set curl request mode to post
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
	}
}

$curl_options = array(
	CURLOPT_URL => 'http://localhost:5000/' .$node,
	CURLOPT_RETURNTRANSFER => 1
);
curl_setopt_array($ch, $curl_options);
$response = curl_exec($ch);
@session_start();
$_SESSION['server_response'] = $response;
header('Location: index.php');

?>
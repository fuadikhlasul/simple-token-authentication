# !/usr/bin/env python
# -*- coding: utf-8 -*-
# system module
import os
import MySQLdb
import tornado.web
import tornado.ioloop
import signal, logging
from tornado.options import options
from database_utils import to_dict

__author__ = 'fuadikhlasul@hotmail.com'

closed = False

# disable default tornado logger
logging.getLogger('tornado.access').propagate = False
# set up our logger
logging.basicConfig(
    filename=os.path.join(os.path.abspath(os.path.dirname(__file__)), 'server_log.log'),
    level=logging.DEBUG,
    format='%(levelname)s: %(message)s - %(asctime)s',
    datefmt='%d/%m/%Y %I:%M:%S %p'
)

def signal_handler():
    global closed
    logging.info('Terminating service...')
    closed = True

def try_exit():
    global closed
    if closed:
        tornado.ioloop.IOLoop.instance().stop()
        logging.info('Service terminated.')

class MainHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.write(':: Simple Token-based Authentication Service ::')

class AuthHandler(tornado.web.RequestHandler):
    def post(self, *args, **kwargs):
        command = self.get_argument('command')
        token = self.get_argument('token')
        found = self.is_token_exist(token)
        logging.info('Client message received with command "%s"' %command.upper())

        if not found:
            logging.info('Token "%s" was not found in database, returning cause=not_found' %token)
            self.write({'response': 'not_found'})
        else:
            token_count = self.get_token_count(token)
            # if command = request
            if 'req' == command.lower().strip():
                # increment kolom count di database
                self.inc_db_token(token, token_count)
                logging.debug('Count value for token "%s" was incremented from %s to %s' %(token, token_count, (token_count+1)))
                self.write({'response': 'increment_count'})
            # if command = acknowledge
            elif 'auth' == command.lower().strip():
                # jika nilai current untuk kolom count lebih dari 3
                logging.info('Attempting authentication process using token "%s"' %token)
                if 3 < token_count:
                    logging.info('Exceed count value, returning cause=exceed_limit_count')
                    self.write({'response': 'exceed_limit_cause'})
                # jika nilai current untuk kolom count kurang dari 3, send token-pair ke client
                else:
                    logging.info('Authentication success, returning token-pair value to client')
                    self.write({'response': self.get_token_pair(token)})
            else:
                logging.error('Invalid request message command!')
                self.write({'response': 'invalid_command'})

    def inc_db_token(self, token, old_count):
        """
        increment nilai kolom count di database berdasarkan token input
        :param token: token string
        :param old_count: nilai count saat ini
        :return: None
        """
        _db = self.get_database()
        cur = _db.cursor()
        new_count = old_count + 1
        sql = "UPDATE `tokens` SET `count`=%s WHERE `token`='%s'" %(new_count, token)

        try:
            cur.execute(sql)
            _db.commit()
        except MySQLdb.MySQLError, e:
            logging.error('Database error encountered while incrementing count value')
            _db.rollback()

        _db.close()

    def get_token_count(self, token):
        """
        mengambil nilai count dari database berdasarkan string token input
        :param token: token string
        :return: count: nila count di database
        """
        _db = self.get_database()
        cur = _db.cursor()
        cur.execute("SELECT `count` FROM `tokens` WHERE `token`='%s'" %token)
        res = cur.fetchone()
        _db.close()
        return res[0]

    def is_token_exist(self, token):
        """
        mengecek apakah token ada di database
        :param token: token string
        :return: boolean (True/False)
        """
        _db = self.get_database()
        cur = _db.cursor()
        sql = "SELECT * FROM `tokens` WHERE `token`='%s'" %token
        cur.execute(sql)
        tokens = [_['token'] for _ in to_dict(cur)]
        return True if tokens else False

    def get_token_pair(self, token):
        _db = self.get_database()
        cur = _db.cursor()
        cur.execute("SELECT `token_pair` FROM `tokens` WHERE `token`='%s'" %token)
        res = cur.fetchone()
        _db.close()
        return res[0]

    def get_database(self):
        """
        get koneksi database mysql
        :return: MySQL database connection object
        """
        return MySQLdb.connect('localhost', 'root', '', 'simple_auth')

class InstallHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        from database_utils import create_table, seed_token_entry
        create_table()
        seed_token_entry()
        self.write({'response': 'database_init_success'})

def make_app():
    return tornado.web.Application([
        (r'/', MainHandler),
        (r'/auth', AuthHandler),
        (r'/install', InstallHandler),
    ])

if __name__ == '__main__':
    # menghandle stop service via keyboard interrupt signal (ctrl+c)
    tornado.options.parse_command_line()
    signal.signal(signal.SIGINT, signal_handler)
    # service menggunakan port 2016
    _port = 5000
    _app = make_app()
    _app.listen(_port)
    tornado.ioloop.PeriodicCallback(try_exit, 100).start()
    # jalankan service
    tornado.ioloop.IOLoop.instance().start()
# !/usr/bin/env python
# -*- coding: utf-8 -*-
# system module
import os, logging, MySQLdb
from bottle import route, run, request
from database_utils import to_dict

__author__ = 'fuadikhlasul@hotmail.com'

# set up our logger
logging.basicConfig(
    filename=os.path.join(os.path.abspath(os.path.dirname(__file__)), 'server_log.log'),
    level=logging.DEBUG,
    format='%(levelname)s: %(message)s - %(asctime)s',
    datefmt='%d/%m/%Y %I:%M:%S %p'
)

@route('/auth', method='POST')
def auth():
    db_ = MySQLdb.connect('localhost', 'root', '', 'simple_auth')
    db_cursor = db_.cursor()
    token = request.forms.get('token')
    command = request.forms.get('command')
    logging.info('Client message received with command "%s"' %command.upper())

    # cek apakah token ada di database atau tidak
    db_cursor.execute("SELECT * FROM `tokens` WHERE `token`='%s'" %token)
    res = [_['token'] for _ in to_dict(db_cursor)]

    if not res:
        # jika token tidak ada di databse, return error_code = 'not_found'
        logging.info('Token "%s" was not found in database, returning cause=not_found' %token)
        return {'response': 'not_found'}
    else:
        # mengambil nilai current count pada kolom count di database berdasarkan token string input
        db_cursor.execute("SELECT `count` FROM `tokens` WHERE `token`='%s'" %token)
        token_count = db_cursor.fetchone()[0]

        if 'req' == command.lower().strip():
            # jika command = request, increment nilai kolom count di database
            try:
                db_cursor.execute("UPDATE `tokens` SET `count`=%s WHERE `token`='%s'" %((token_count + 1), token))
                db_.commit()
                db_.close()
                logging.debug('Count value for token "%s" was incremented from %s to %s' %(token, token_count, (token_count+1)))
                return {'response': 'increment_count'}
            except MySQLdb.MySQLError, e:
                db_.rollback()
                db_.close()
                logging.error('Database error encountered while incrementing count value')
                return {'response': 'database_error_while_increment_count'}
        elif 'auth' == command.lower().strip():
            # jika command = acknowledge, cek nilai count di database
            logging.info('Attempting authentication process using token "%s"' %token)
            if 3 < token_count:
                # jika nilai count > 3, return error_code = 'exceed_limit_cause'
                db_.close()
                logging.info('Exceed count value, returning cause=exceed_limit_count')
                return {'response': 'exceed_limit_cause'}
            else:
                # jika count < 3, return token_pair
                db_cursor.execute("SELECT `token_pair` FROM `tokens` WHERE `token`='%s'" %token)
                token_pair = db_cursor.fetchone()[0]
                db_.close()
                logging.info('Authentication success, returning token-pair value to client')
                return {'response': token_pair}
        else:
            logging.error('Invalid request message command!')
            return {'response': 'invalid_command_argument'}

@route('/install')
def prepare_database():
    from database_utils import create_table, seed_token_entry
    create_table()
    seed_token_entry()
    return {'response': 'database_init_success'}

if __name__ == '__main__':
    run(host='localhost', port='5000', debug=True)